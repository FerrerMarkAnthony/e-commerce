import Banner from "../components/Banner";
import { Container, Row, Col } from "react-bootstrap";
import Highlights from "../components/Highlights";

export default function Home() {
  const data = {
    title: "CLEANING MATERIALS",
    content: "Clean Clean Clean",
    destination: "/products",
    label: "BUY NOW",
  };

  return (
    <Container>
      <Row className="justify-content-center text-center">
        <Col xs={12} md={10}>
          <Banner data={data} />
        </Col>
      </Row>

      <Row className="justify-content-center">
        <Col xs={12} md={10}>
          <Highlights />
        </Col>
      </Row>
      {/*<CourseCard />*/}
    </Container>
  );
}
