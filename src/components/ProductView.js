import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function CourseView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const product = (productId) => {
    const userId = user.id;
    const productName = name;

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
        userId: userId,
        productName: productName,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Successfully",
            icon: "success",
            text: "You have successfully Purchase.",
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  const handleIncrement = () => {
    setQuantity(quantity + 1);
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  return (
    <div id="checkout">
      <Container className="mt-5">
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card className="shadow-sm my-3">
              <Card.Body>
                <Card.Title className="title">{name}</Card.Title>
                <Card.Subtitle className="mb-3 text-muted">
                  Description:
                </Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle className="mt-3">Price:</Card.Subtitle>
                <Card.Text className="mb-3 font-weight-bold">
                  Php {price}
                </Card.Text>
                <Card.Subtitle className="mt-3">Quantity:</Card.Subtitle>

                <div className="input-group mb-3">
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={handleDecrement}
                  >
                    -
                  </button>
                  <input
                    type="number"
                    className="form-control text-center"
                    value={quantity}
                  />
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={handleIncrement}
                  >
                    +
                  </button>
                </div>

                <Card.Subtitle className="mt-3">Subtotal:</Card.Subtitle>
                <Card.Text className="mb-3 font-weight-bold">
                  Php {price * quantity}
                </Card.Text>

                {user.id !== null ? (
                  <div className="text-center">
                    <Button
                      variant="primary"
                      block
                      onClick={() => product(productId)}
                    >
                      Checkout
                    </Button>
                  </div>
                ) : (
                  <Link className="btn btn-danger btn-block" to="/login">
                    Buy now
                  </Link>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
