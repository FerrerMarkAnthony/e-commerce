// import { Card } from "react-bootstrap";
// import { Link } from "react-router-dom";
// import { Container } from "react-bootstrap";

// export default function ProductCard({ product }) {
//   const { _id, name, description, price } = product;

//   return (
//     <Container>
//       <Card>
//         <Card.Body>
//           <Card.Title>{name}</Card.Title>
//           <Card.Subtitle>Description:</Card.Subtitle>
//           <Card.Text>{description}</Card.Text>
//           <Card.Subtitle>Price:</Card.Subtitle>
//           <Card.Text>{price}</Card.Text>
//           <Link className="btn btn-primary" to={`/products/${_id}`}>
//             Details
//           </Link>
//         </Card.Body>
//       </Card>
//     </Container>
//   );
// }

// import { Card, Container, Button } from "react-bootstrap";
// import { Link } from "react-router-dom";

// export default function ProductCard({ product }) {
//   const { _id, name, description, price } = product;

//   return (
//     <Container>
//       <Card>
//         <Card.Body>
//           <Card.Title>{name}</Card.Title>
//           <Card.Subtitle className="mb-2 text-muted">
//             Description:
//           </Card.Subtitle>
//           <Card.Text>{description}</Card.Text>
//           <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
//           <Card.Text>{price}</Card.Text>
//           <Link className="btn btn-primary" to={`/products/${_id}`}>
//             Details
//           </Link>
//         </Card.Body>
//       </Card>
//     </Container>
//   );
// }

import { Card, Container, Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({ product }) {
  const { _id, name, description, price } = product;

  return (
    <Container className="mb-5">
      <Card>
        <Card.Body>
          <Table striped bordered hover>
            <tbody>
              <tr>
                <td>
                  <strong>Name:</strong>
                </td>
                <td>{name}</td>
              </tr>
              <tr>
                <td>
                  <strong>Description:</strong>
                </td>
                <td>{description}</td>
              </tr>
              <tr>
                <td>
                  <strong>Price:</strong>
                </td>
                <td>{price}</td>
              </tr>
            </tbody>
          </Table>
          <Link className="btn btn-primary" to={`/products/${_id}`}>
            Details
          </Link>
        </Card.Body>
      </Card>
    </Container>
  );
}
