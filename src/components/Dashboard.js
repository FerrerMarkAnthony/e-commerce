// import React, { useState } from "react";
// import { Table } from "react-bootstrap";

// function ExampleTable() {
//   const [products, setProducts] = useState([]);
//   const [orders, setOrders] = useState([]);

//   const addProduct = (name, description, price, availability, action) => {
//     const newProduct = { name, description, price, availability, action };
//     setProducts([...products, newProduct]);
//   };

//   const placeOrder = () => {
//     setOrders([...orders, products]);
//     setProducts([]);
//   };

//   return (
//     <>
//       <Table striped bordered hover>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Availability</th>
//             <th>Action</th>
//           </tr>
//         </thead>
//         <tbody>
//           {products.map((product, index) => (
//             <tr key={index}>
//               <td>{product.name}</td>
//               <td>{product.description}</td>
//               <td>{product.price}</td>
//               <td>{product.availability}</td>
//               <td>{product.action}</td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>
//       <div style={{ margin: "20px 0" }}>
//         <button
//           style={{ marginRight: "10px" }}
//           onClick={() =>
//             addProduct(
//               "Product 1",
//               "Description 1",
//               10,
//               "In stock",
//               "Add to cart"
//             )
//           }
//         >
//           Add Product
//         </button>
//         <button style={{ marginLeft: "10px" }} onClick={placeOrder}>
//           Place Order
//         </button>
//       </div>
//       {orders.length > 0 && (
//         <div>
//           <h2>Orders:</h2>
//           {orders.map((order, index) => (
//             <div key={index}>
//               <h4>Order #{index + 1}</h4>
//               <Table striped bordered hover>
//                 <thead>
//                   <tr>
//                     <th>Name</th>
//                     <th>Description</th>
//                     <th>Price</th>
//                     <th>Availability</th>
//                     <th>Action</th>
//                   </tr>
//                 </thead>
//                 <tbody>
//                   {order.map((product, index) => (
//                     <tr key={index}>
//                       <td>{product.name}</td>
//                       <td>{product.description}</td>
//                       <td>{product.price}</td>
//                       <td>{product.availability}</td>
//                       <td>{product.action}</td>
//                     </tr>
//                   ))}
//                 </tbody>
//               </Table>
//             </div>
//           ))}
//         </div>
//       )}
//     </>
//   );
// }

// export default ExampleTable;

// import React, { useState, useEffect } from "react";
// import { Table } from "react-bootstrap";

// function ExampleTable() {
//   const [products, setProducts] = useState([]);
//   const [orders, setOrders] = useState([]);

//   useEffect(() => {
//     fetch("https://localhost/products/")
//       .then((response) => response.json())
//       .then((data) => {
//         setProducts(data);
//       });
//   }, []);

//   const addProduct = (name, description, price, availability, action) => {
//     const newProduct = { name, description, price, availability, action };
//     setProducts([...products, newProduct]);
//   };

//   const placeOrder = () => {
//     setOrders([...orders, products]);
//     setProducts([]);
//   };

//   return (
//     <>
//       <Table striped bordered hover>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Availability</th>
//             <th>Action</th>
//           </tr>
//         </thead>
//         <tbody>
//           {products.map((product, index) => (
//             <tr key={index}>
//               <td>{product.name}</td>
//               <td>{product.description}</td>
//               <td>{product.price}</td>
//               <td>{product.availability}</td>
//               <td>{product.action}</td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>
//       <div style={{ margin: "20px 0" }}>
//         <button
//           style={{ marginRight: "10px" }}
//           onClick={() =>
//             addProduct(
//               "Product 1",
//               "Description 1",
//               10,
//               "In stock",
//               "Add to cart"
//             )
//           }
//         >
//           Add Product
//         </button>
//         <button style={{ marginLeft: "10px" }} onClick={placeOrder}>
//           Show User Order
//         </button>
//       </div>
//       {orders.length > 0 && (
//         <div>
//           <h2>Orders:</h2>
//           {orders.map((order, index) => (
//             <div key={index}>
//               <h4>Order #{index + 1}</h4>
//               <Table striped bordered hover>
//                 <thead>
//                   <tr>
//                     <th>Name</th>
//                     <th>Description</th>
//                     <th>Price</th>
//                     <th>Availability</th>
//                     <th>Action</th>
//                   </tr>
//                 </thead>
//                 <tbody>
//                   {order.map((product, index) => (
//                     <tr key={index}>
//                       <td>{product.name}</td>
//                       <td>{product.description}</td>
//                       <td>{product.price}</td>
//                       <td>{product.availability}</td>
//                       <td>{product.action}</td>
//                     </tr>
//                   ))}
//                 </tbody>
//               </Table>
//             </div>
//           ))}
//         </div>
//       )}
//     </>
//   );
// }

// export default ExampleTable;

// import React, { useState, useEffect } from "react";
// import { Table, Form, Button } from "react-bootstrap";

// function ExampleTable() {
//   const [products, setProducts] = useState([]);
//   const [orders, setOrders] = useState([]);
//   const [formData, setFormData] = useState({
//     name: "",
//     description: "",
//     price: "",
//     availability: "",
//     action: "",
//   });

//   useEffect(() => {
//     const token = localStorage.getItem("token");
//     if (token) {
//       const headers = { Authorization: `Bearer ${token}` };
//       fetch("https://localhost/products", { headers })
//         .then((response) => {
//           if (!response.ok) {
//             throw new Error(response.statusText);
//           }
//           return response.json();
//         })
//         .then((data) => {
//           setProducts(data);
//         })
//         .catch((error) => {
//           console.log(error);
//         });
//     }
//   }, []);

//   const handleInputChange = (event) => {
//     const { name, value } = event.target;
//     setFormData((prevFormData) => ({
//       ...prevFormData,
//       [name]: value,
//     }));
//   };

//   const handleSubmit = (event) => {
//     event.preventDefault();
//     const newProduct = { ...formData };
//     setProducts([...products, newProduct]);
//     setFormData({
//       name: "",
//       description: "",
//       price: "",
//       availability: "",
//       action: "",
//     });
//   };

//   const placeOrder = () => {
//     setOrders([...orders, products]);
//     setProducts([]);
//   };

//   return (
//     <>
//       <Table striped bordered hover>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Availability</th>
//             <th>Action</th>
//           </tr>
//         </thead>
//         <tbody>
//           {products.map((product, index) => (
//             <tr key={index}>
//               <td>{product.name}</td>
//               <td>{product.description}</td>
//               <td>{product.price}</td>
//               <td>{product.availability}</td>
//               <td>{product.action}</td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>
//       <div style={{ margin: "20px 0" }}>
//         <Form onSubmit={handleSubmit}>
//           <Form.Group controlId="productName">
//             <Form.Label>Name</Form.Label>
//             <Form.Control
//               type="text"
//               placeholder="Enter product name"
//               name="name"
//               value={formData.name}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productDescription">
//             <Form.Label>Description</Form.Label>
//             <Form.Control
//               type="text"
//               placeholder="Enter product description"
//               name="description"
//               value={formData.description}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productPrice">
//             <Form.Label>Price</Form.Label>
//             <Form.Control
//               type="number"
//               placeholder="Enter product price"
//               name="price"
//               value={formData.price}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productAvailability">
//             <Form.Label>Availability</Form.Label>
//             <Form.Control
//               type="number"
//               placeholder="Enter product availability"
//               name="availability"
//               value={formData.availability}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productAction">
//             <Form.Label>Action</Form.Label>
//             <Form.Control
//               as="select"
//               name="action"
//               value={formData.action}
//               onChange={handleInputChange}
//             >
//               <option value="">Select action</option>
//               <option value="add">Add</option>
//               <option value="edit">Edit</option>
//               <option value="delete">Delete</option>
//             </Form.Control>
//           </Form.Group>
//           <Button variant="primary" type="submit">
//             Submit
//           </Button>
//         </Form>
//       </div>
//       <Button variant="primary" onClick={placeOrder}>
//         Show Order
//       </Button>
//     </>
//   );
// }

// export default ExampleTable;

// import React, { useState, useEffect } from "react";
// import { Table, Form, Button } from "react-bootstrap";

// function ExampleTable() {
//   const [products, setProducts] = useState([]);
//   const [formData, setFormData] = useState({
//     name: "",
//     description: "",
//     price: "",
//   });
//   const [orders, setOrders] = useState([]);

//   useEffect(() => {
//     const token = localStorage.getItem("token");
//     if (token) {
//       const headers = { Authorization: `Bearer ${token}` };
//       fetch("https://localhost/products/addProduct", { headers })
//         .then((response) => {
//           if (!response.ok) {
//             throw new Error(response.statusText);
//           }
//           return response.json();
//         })
//         .then((data) => {
//           setProducts(data);
//         })
//         .catch((error) => {
//           console.log(error);
//         });
//     }
//   }, []);

//   const handleInputChange = (event) => {
//     const { name, value } = event.target;
//     setFormData((prevFormData) => ({
//       ...prevFormData,
//       [name]: value,
//     }));
//   };

//   const handleSubmit = (event) => {
//     event.preventDefault();
//     const newProduct = { ...formData };
//     setProducts([...products, newProduct]);
//     setFormData({
//       name: "",
//       description: "",
//       price: "",
//     });
//   };

//   const placeOrder = () => {
//     setOrders([...orders, products]);
//     setProducts([]);
//   };

//   return (
//     <>
//       <Table striped bordered hover>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Availability</th>
//             <th>Action</th>
//           </tr>
//         </thead>
//         <tbody>
//           {products.map((product, index) => (
//             <tr key={index}>
//               <td>{product.name}</td>
//               <td>{product.description}</td>
//               <td>{product.price}</td>
//               <td>{product.availability}</td>
//               <td>{product.action}</td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>
//       <div style={{ margin: "20px 0" }}>
//         <Form onSubmit={handleSubmit}>
//           <Form.Group controlId="productName">
//             <Form.Label>Name</Form.Label>
//             <Form.Control
//               type="text"
//               placeholder="Enter product name"
//               name="name"
//               value={formData.name}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productDescription">
//             <Form.Label>Description</Form.Label>
//             <Form.Control
//               type="text"
//               placeholder="Enter product description"
//               name="description"
//               value={formData.description}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Form.Group controlId="productPrice">
//             <Form.Label>Price</Form.Label>
//             <Form.Control
//               type="number"
//               placeholder="Enter product price"
//               name="price"
//               value={formData.price}
//               onChange={handleInputChange}
//             />
//           </Form.Group>
//           <Button variant="primary" type="submit">
//             Submit
//           </Button>
//         </Form>
//       </div>
//       <Button variant="primary" onClick={placeOrder}>
//         Show Order
//       </Button>
//     </>
//   );
// }

import { useState, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AddProduct() {
  const { user } = useContext(UserContext);

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [isActive, setIsActive] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name,
        price,
        description,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.error) {
          Swal.fire({
            title: "Add Product Failed",
            icon: "error",
            text: data.error,
          });
        } else {
          Swal.fire({
            title: "Add Product Successful",
            icon: "success",
            text: "Product has been added.",
          });
        }
      });

    setName("");
    setPrice("");
    setDescription("");
    setIsActive(false);
  }

  function handleInputChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.value;

    switch (name) {
      case "name":
        setName(value);
        break;
      case "price":
        setPrice(value);
        break;
      case "description":
        setDescription(value);
        break;
      default:
        break;
    }
  }

  function validateForm() {
    return name.length > 0 && price.length > 0 && description.length > 0;
  }

  return (
    <Container id="contact2">
      {user.isAdmin ? (
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="name">
            <Form.Label id="form">Product Name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              value={name}
              onChange={handleInputChange}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label id="form">Price</Form.Label>
            <Form.Control
              type="number"
              name="price"
              value={price}
              onChange={handleInputChange}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label id="form">Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              name="description"
              value={description}
              onChange={handleInputChange}
              required
            />
          </Form.Group>

          {validateForm() ? (
            <Button variant="success" type="submit" id="submitBtn">
              Add Product
            </Button>
          ) : (
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Add Product
            </Button>
          )}
        </Form>
      ) : (
        <h3>Access Denied. Only admins can add products.</h3>
      )}
    </Container>
  );
}
